package com.namor.Concurrency.ForkJoin;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class BigIntFibonacci {
    public static void main(String[] args) {
        long mills = System.currentTimeMillis();
        System.out.println(fibonacci(10000));
        System.out.println(System.currentTimeMillis() - mills + " ms");
    }

    private static final Map<Integer, BigInteger> cache = new HashMap<>();
    static {
        cache.put(0, BigInteger.ZERO);
        cache.put(1, BigInteger.ONE);
    }

    public static BigInteger fibonacci(int n) {
        if (cache.containsKey(n)) {
            return cache.get(n);
        } else {
            BigInteger a = fibonacci(n - 1);
            BigInteger b = fibonacci(n - 2);

            cache.put(n, a.add(b));
            return cache.get(n);
        }
    }
}
