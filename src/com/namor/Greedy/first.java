package com.namor.Greedy;

import java.util.*;

public class first {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            int n = in.nextInt();
            List<Line> lines = new ArrayList<>();

            Set<Integer> points = new HashSet<>();
            //TreeSet<Line> lineSet = new TreeSet<>();
            List<Line> lineList = new ArrayList<>();

            for (int i = 0; i < n; i++) {
                lines.add(new Line(in.nextInt(), in.nextInt()));
            }

            lines.sort(Comparator.comparingInt(Line::getA));

            for (Line l : lines) {
                Line intersect = l;
                if (!l.isMarked()) {
                    for (Line line : lines) {
                        if (!line.isMarked() && intersect.isIntersect(line)) {
                            intersect = intersect.intersection(line);
                            line.mark();
                        }
                    }
                    l.mark();
                }
                boolean add = true;
                for (Line line : lineList) {
                    if (line.compareTo(intersect) == 0) {
                        add = false;
                    }
                }
                if (add) {
                    //lineSet.add(intersect);
                    lineList.add(intersect);
                }
            }

            System.out.println(lineList.size());
            for (Line l : lineList) {
                System.out.print(l.getB() + " ");
            }
        }
    }

    public static void maino(String[] args) {
        Line l = new Line(6, 6);
        Line la = new Line(6, 8);

        Set<Line> lineSet = new TreeSet<>();
        lineSet.add(la);
        lineSet.add(l);

        System.out.println(lineSet);

        System.out.println(l.compareTo(la));
        System.out.println(la.compareTo(l));
    }

    static class Line implements Comparable<Line> {
        public int getA() {
            return a;
        }

        public int getB() {
            return b;
        }

        private int a;
        private int b;

        public boolean isMarked() {
            return marked;
        }

        private boolean marked;

        public void mark() {
            marked = true;
        }

        public Line(int a, int b) {
            this.a = a;
            this.b = b;
            marked = false;
        }

        private boolean contains(int x) {
            if (a <= x && x <= b) {
                return true;
            } else {
                return false;
            }
        }

        public boolean isIntersect(Line right) {
            return Math.min(b, right.b) - Math.max(a, right.a) >= 0;
        }

        public Line intersection(Line right) {
            if (isIntersect(right)) {
                return new Line(Math.max(a, right.a), Math.min(b, right.b));
            }
            else
                return new Line(0, 0);
        }

        @Override
        public String toString() {
            return "(" + a + "," + b + ")";
        }

        @Override
        public int compareTo(Line o) {
            if (a == o.a || b == o.b)
                return 0;
            if (isIntersect(o))
                return 0;
            else if (b - a > o.b - o.a)
                return 1;
            else
                return -1;
        }
    }
}
