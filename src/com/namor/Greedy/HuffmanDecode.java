package com.namor.Greedy;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HuffmanDecode {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            int n = in.nextInt();
            int strLen = in.nextInt();

            Map<Character, String> charCodes = new HashMap<>();
            for (int i = 0; i < n; i++) {
                char ch = in.next().charAt(0);
                String code = in.next();

                in.nextLine();
                charCodes.put(ch, code);
            }

            String encoded = in.nextLine();
            StringBuilder decoded = new StringBuilder();

            while (!encoded.isEmpty()) {
               for (Map.Entry<Character, String> e : charCodes.entrySet()) {
                   String code = e.getValue();
                   if (encoded.length() >= code.length()) {
                       if (encoded.substring(0, code.length()).equals(code)) {
                           decoded.append(e.getKey());
                           encoded = encoded.substring(code.length());
                       }
                   }
               }
            }

            System.out.println(decoded);
        }
    }
}
