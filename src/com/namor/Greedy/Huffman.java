package com.namor.Greedy;

import java.util.*;

public class Huffman {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in)) {
            String inputString = in.nextLine();
            Map<Character, Integer> symbolRate = new HashMap<>();

            for (char c : inputString.toCharArray()) {
                symbolRate.merge(c, 1, Integer::sum);
            }

            PriorityQueue<TreeNode<CharRate>> nodes = new PriorityQueue<>();
            for (Map.Entry<Character, Integer> entry : symbolRate.entrySet()) {
                CharRate charRate = new CharRate(entry.getKey(), entry.getValue());
                nodes.add(new TreeNode<>(charRate));
            }

            final TreeNode<CharRate> tree = getTree(nodes);

            Map<Character, String> charCodes = new HashMap<>();
            genCodes(tree, charCodes);

            //output
            StringBuilder outputString = new StringBuilder();
            for (char c : inputString.toCharArray()) {
                outputString.append(charCodes.get(c));
            }

            System.out.println(charCodes.size() + " " + outputString.length());
            charCodes.forEach((c, s) -> System.out.println(c + ": " + s));
            System.out.println(outputString);
        }
    }

    public static void genCodes(TreeNode<CharRate> tree, Map<Character, String> codes) {
        if (tree.hasChild()) {
            genCodes(tree, "", codes);
        } else {
            Character ch = tree.getValue().getChar();
            codes.put(ch, "0");
        }
    }

    private static void genCodes(TreeNode<CharRate> tree, String code, Map<Character, String> codes) {
        if (tree.getRight() != null) {
            genCodes(tree.getRight(), code + "1", codes);
        }
        if (tree.getLeft() != null) {
            genCodes(tree.getLeft(), code + "0", codes);
        }
        if (!tree.hasChild()) {
            Character ch = tree.getValue().getChar();
            codes.put(ch, code);
        }
    }

    public static TreeNode<CharRate> getTree(PriorityQueue<TreeNode<CharRate>> nodes) {
        while (nodes.size() > 1) {
            TreeNode<CharRate> leftMin = nodes.poll();
            TreeNode<CharRate> rightMin = nodes.poll();

            CharRate newCharRate =
                    new CharRate(null, leftMin.getValue().getRate() + rightMin.getValue().getRate());

            TreeNode<CharRate> newNode = new TreeNode<>(newCharRate);

            newNode.setLeft(leftMin);
            newNode.setRight(rightMin);

            nodes.add(newNode);
        }

        return nodes.poll();
    }

    static class CharRate implements Comparable<CharRate>{
        Character c;
        int rate;

        public CharRate(Character c, int rate) {
            this.c = c;
            this.rate = rate;
        }

        public Character getChar() {
            return c;
        }

        public int getRate() {
            return rate;
        }

        @Override
        public int compareTo(CharRate o) {
            return Integer.compare(rate, o.rate);
        }

        @Override
        public String toString() {
            return c + ": " + rate;
        }
    }

    static class TreeNode<T extends Comparable<T>> implements Comparable<TreeNode<T>> {
        TreeNode<T> parent;
        TreeNode<T> left;
        TreeNode<T> right;
        T value;

        public boolean hasChild() {
            return left != null || right != null;
        }

        public TreeNode(T value) {
            this.value = value;
        }

        public TreeNode<T> getParent() {
            return parent;
        }

        public void setParent(TreeNode<T> parent) {
            this.parent = parent;
        }

        public TreeNode<T> getLeft() {
            return left;
        }

        public void setLeft(TreeNode<T> left) {
            this.left = left;
            left.setParent(this);
        }

        public TreeNode<T> getRight() {
            return right;
        }

        public void setRight(TreeNode<T> right) {
            this.right = right;
            right.setParent(this);
        }

        public T getValue() {
            return value;
        }

        @Override
        public int compareTo(TreeNode<T> o) {
            return getValue().compareTo(o.getValue());
        }
    }
}
