package com.namor;

import java.util.Scanner;

public class FibLastDigit {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt() + 1;
        int[] array = new int[n];

        array[0] = 0;
        array[1] = 1;

        for (int i = 2; i < array.length; i++) {
            array[i] = (array[i - 1] + array[i - 2]) % 10;
        }

        System.out.println(array[array.length - 1]);
    }
}
