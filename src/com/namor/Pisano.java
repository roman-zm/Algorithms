package com.namor;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Pisano {
    public static LinkedList<Integer> findPisano(int m) {
        LinkedList<Integer> pisanoList = new LinkedList<>();
        if (m == 0)
            return pisanoList;

        pisanoList.add(0);
        if (m == 1) {
            return pisanoList;
        }

        pisanoList.add(1);
        pisanoList.add(1);
        if (m == 2) {
            return pisanoList;
        }

        for (int i = 3; i <= 6 * m; i++) {
            int next = (pisanoList.get(i - 1) + pisanoList.get(i - 2)) % m;
            pisanoList.add(next);

            if (pisanoList.get(i - 2).equals(0) && pisanoList.get(i - 1).equals(1)
                    && pisanoList.get(i).equals(1)) {

                pisanoList.removeLast();
                pisanoList.removeLast();
                pisanoList.removeLast();
                return pisanoList;
            }
        }

        return pisanoList;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        long n = in.nextLong();
        int m = in.nextInt();
        List<Integer> pisano = findPisano(m);

        System.out.println(pisano.get((int) (n % pisano.size())));
    }
}
