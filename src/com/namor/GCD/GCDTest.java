package com.namor.GCD;


public class GCDTest {
    public static void main(String[] args) {
        long a = 1253567342636353134L;
        long b = 384369432795680327L;

        long start = System.currentTimeMillis();
        System.out.println(gcdRecursive(a, b));
        System.out.println(System.currentTimeMillis() - start + " ms");

        start = System.currentTimeMillis();
        int a1 = 124135253;
        int b1 = 6536453;
        System.out.println(gcd(a1, b1));
        System.out.println(System.currentTimeMillis() - start + " ms");
    }

    public static long gcdRecursive(long a, long b) {
        System.out.println(a + " " + b);
        if (a == 0)
            return b;
        if (b == 0)
            return a;

        if (a >= b)
            return gcdRecursive(a % b, b);
        else
            return gcdRecursive(a, b % a);
    }

    public static long gcd(long a, long b) {
        while (true) {
            System.out.println(a + " " + b);
            if (a == 0)
                return b;
            if (b == 0)
                return a;

            if (a >= b) {
                a %= b;
            } else {
                b %= a;
            }
        }
    }
}
