package com.namor;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final double phi = (1 + Math.sqrt(5)) / 2;
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int Fn = (int) ((Math.pow(phi, n) - Math.pow(-phi, -n)) / (2*phi - 1));

        System.out.println(Fn);
    }
}
